#!/usr/bin/python -tt

''' 
--Description--

You've been taking some classes at a local university. Unfortunately, your theory-of-under-water-basket-weaving professor is really boring. 
He's also very nosy. In order to pass the time during class, you like sharing notes with your best friend sitting across the aisle. 
Just in case your professor intercepts any of your notes, you've decided to encrypt them.

To make things easier for yourself, you're going to write a program which will encrypt the notes for you. You've decided a transposition 
cipher is probably the best suited method for your purposes.

A transposition cipher is "a method of encryption by which the positions held by units of plaintext (which are commonly characters or groups 
of characters) are shifted according to a regular system, so that the ciphertext constitutes a permutation of the plaintext" (En.wikipedia.org, 2018).

Specifically, we will be implementing a type of route cipher today. In a route cipher the text you want to encrypt is written out in a grid, 
and then arranged in a given pattern. The pattern can be as simple or complex as you'd like to make it.

--Task--

For our purposes today, your program should be able to accommodate two input paramters: Grid Dimensions, and Clockwise or Counterclockwise Rotation. 
To make things easier, your program need only support the Spiral route from outside to inside.

Link to challenge
 https://www.reddit.com/r/dailyprogrammer/comments/8n8tog/20180530_challenge_362_intermediate_route/?utm_content=title&utm_medium=front&utm_source=reddit&utm_name=dailyprogrammer

TODO, implement counter-clockwise and decryption
 To improve, no reason to do if statements when loop is greater than 1, just start loop at 0
'''
import string
import argparse


ap = argparse.ArgumentParser()
ap.add_argument("-c", "--cleartext", required=True,
	help="Cleartext you wish to encrypt")
ap.add_argument("-g", "--grid", required=True, nargs='+', type=int,
	help="Number of columns and rows to split text, ie --gid 2 2")
ap.add_argument("-r", "--rotation", required=True,
	help="Direction to rotate text")
args = vars(ap.parse_args())

def get_string(text):
	mystring = ''
	for entry in text.upper():
		if entry in string.ascii_uppercase:
			mystring += entry

	return mystring

def get_grid(text,grid):
	mylist = []
	start = 0
	end = grid[0]
	for a in range(grid[1]):
		mylist.append(text[start:end])
		start += grid[0]
		end += grid[0]

	mylist = add_buffer(mylist)
	return mylist

def add_buffer(grid):
	mylen = len(grid)
	mytext = ''
	for a in range(mylen):
		if len(grid[a]) != len(grid[0]):
			mytext += grid[a] + ('X' * (len(grid[0]) - len(grid[a])))
			grid[a] = mytext
	return grid
	
'''def rotate(textlist,grid,rotation):
	if rotation == 'clockwise':
		length = grid[0]
		enc = 0
	elif rotation == 'counter-clockwise':
'''

def get_right(mylist,loopnumber):
	rstring = ''
	if loopnumber > 1:
		mynum = loopnumber - 1
		for a in mylist[mynum:-mynum]:
			rstring += a[-loopnumber]
	else:
		for a in mylist:
			rstring += a[-1]
	return rstring

def get_bottom(mylist, loopnumber):
	botstring = ''
	if loopnumber == 1:
		for a in mylist[-loopnumber][:-loopnumber][::-1]:
			botstring += a
	else:
		for a in mylist[-loopnumber][(loopnumber-1):-loopnumber][::-1]:
			botstring += a
	return botstring

def get_left(mylist, loopnumber):
	leftstring = ''
	if loopnumber == 1:
		for a in mylist[:-loopnumber][::-1]:
			leftstring += a[0]
	else:
		for a in mylist[loopnumber - 1 : -loopnumber][::-1]:
			leftstring += a[loopnumber - 1]

	return leftstring

def get_top(mylist, loopnumber):
	rightstring = '' 
	if loopnumber == 1:
		for a in mylist[0][loopnumber:-loopnumber]:
			rightstring += a
	else:
		for a in mylist[loopnumber - 1][loopnumber:-loopnumber]:
			rightstring += a
	return rightstring

def get_enc(mylist):
	tot = 0
	for a in mylist:
		tot += len(a)

	complete  = ''
	loop = 1
	while exit != 1:
		complete += get_right(mylist,loop)
		if len(complete) == tot: 
			break

		complete += get_bottom(mylist,loop)
		if len(complete) == tot: 
			break

		complete += get_left(mylist,loop)
		if len(complete) == tot: 
			break

		complete += get_top(mylist,loop)
		if len(complete) == tot: 
			break
		
		loop += 1
	return complete

def main():
	rawinput = args["cleartext"]
	rotate = tuple(args["grid"])
	rot = args["rotation"]
	print "Processing",rawinput, rotate, rot
	upptext = get_string(rawinput)
	grid = get_grid(upptext, rotate)
	
	new = get_enc(grid)
	print new

if __name__ == '__main__':
	main()
